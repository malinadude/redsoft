import $ from 'jquery';
window.$ = window.jQuery = $;
import 'slick-carousel';
import 'jquery-validation';

$(document).ready(function () {
    // таймер закрытия popup
    var globalPopupTimeout;

    // чистим модалку, чтобы пользователь не увидел ничего лишнего
    function clearPopupTimeOut() {
        if (globalPopupTimeout) {
            clearTimeout(globalPopupTimeout);

            $('.modal .status').css('display', 'none');
        }
    }

    // подключение jQuery Validation
    validateForm($('.submit-request'));

    // слайдер в шапке
    $('.header__slider').slick({
        arrows: false,
        dots: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 5000,
        pauseOnHover: true,
    });

    // активный/неактивный пункт меню
    $('body').on('click', '.header-menu__item:not(.button)', function () {
        $(this).siblings().removeClass('active');
        $(this).addClass('active');
    });

    // popup логика
    $('body').on('click', '.button_modal', function () {
        let call_modal = '.' + $(this).attr('call-modal');
        let active = 'active';
        let activeForm = $('form' + call_modal);

        if (call_modal != '') {
            $('form:not(' + call_modal + ')').removeClass(active);
            activeForm.addClass(active);

            if (activeForm.attr('style')) {
                activeForm.attr('style', '');
            }
            activeForm.removeClass('hidden');
            activeForm.addClass(active);

            $('body').addClass('disable');
            $('.modal').fadeIn(400, 'swing', function () {
                $(this).addClass(active);
            });
        }
    });
    $('body').on('click', '.overlay', function () {
        $('.modal').fadeOut(400, 'swing', function () {
            clearPopupTimeOut();

            $(this).removeClass('active');
        });
        $('body').removeClass('disable');
    });

    // валидация формы
    function validateForm(form) {
        $(form).validate({
            rules: {
                submit_request_name: 'required',
                submit_request_surname: 'required',
                submit_request_patronymic: 'required',
                submit_request_email: {
                    required: true,
                    email: true,
                },
                submit_request_phone: {
                    required: true,
                    number: true,
                },
            },
            messages: {
                submit_request_name: "Поле 'Имя' обязательно к заполнению",
                submit_request_surname: "Поле 'Фамилия' обязательно к заполнению",
                submit_request_patronymic: "Поле 'Отчество' обязательно к заполнению",
                submit_request_email: {
                    required: "Поле 'Email' обязательно к заполнению",
                    email: 'Необходим формат адреса email'
                },
                submit_request_phone: {
                    required: "Поле 'Телефон' обязательно к заполнению",
                    number: 'Необходим формат номера телефона'
                }
            },
            submitHandler: function (form) {
                $.ajax({
                    type: 'POST',
                    url: '/form.php',
                    data: $(form).serialize(),
                    async: false,
                    dataType: "html",
                    success: function (result) {
                        let statusElement = $(form).parent().find('.status');

                        statusElement.text(result);

                        $(form).fadeOut(400, 'swing', function () {
                            $(form).addClass('hidden');
                            $(form)[0].reset();
                            statusElement.fadeIn();
                        });

                        globalPopupTimeout = setTimeout(function () {
                            $(form).closest('.modal').find('.overlay').trigger('click');
                            statusElement.fadeOut(400, 'swing', function () {
                                $(form).removeClass('hidden');
                            });
                        }, 3000)
                    }
                });
            }
        });
    };

    // форма подписки
    $('form').on('submit', function (e) {
        e.preventDefault();

        if ($(this).hasClass('subscribe-form')) {
            return false;
        }

        validateForm(this);
    });
});
