    const mix = require('laravel-mix');
    var BrowserSyncPlugin = require('browser-sync-webpack-plugin');
    const ImageminPlugin = require('imagemin-webpack-plugin').default;
    const CopyWebpackPlugin = require('copy-webpack-plugin');
    const imageminMozjpeg = require('imagemin-mozjpeg');

    // require('custom-env').env();

    mix.webpackConfig({
        plugins: [
            // копирую для публикации на прод
            new CopyWebpackPlugin([
                {
                    from: 'src/images',
                    to: 'images',
                },
                {
                    from: 'src/fonts',
                    to: 'fonts',
                }
            ]),
            new BrowserSyncPlugin(
                {
                    host: 'localhost',
                    server: { baseDir: './' },
                    files: 'index.html',
                }, {
                    name: 'WebPackStarter',
                    reload: false,
                }
            ),
            new ImageminPlugin({
                test: /\.(jpe?g|png|gif|svg)$/i,
                plugins: [
                    imageminMozjpeg({
                        quality: 80,
                    })
                ]
            })
        ],
    });

    mix.options({
        processCssUrls: false,
        autoprefixer: {
            enabled: true,
            options: {
                browsers: ['last 2 versions', '> 1%'],
                cascade: true,
                grid: true,
            }
        }
    })
    .setPublicPath('public')
    .sass('src/scss/main.scss', 'css/stylesheets.css')
    .js('src/js/app.js', 'js/scripts.compiled.js')
    .extract(['jquery']);
